# Cobrainer iAPI-Server Specs
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [v3.2.0-70] - 2018-11-22

### Added
* [EI-1194] PATCH `/expertises/{expertiseId}` to modify Expertise title
* [EI-1195] GET `/expertise/{expertiseId}/labels` to get Expertise labels, only for Admin user
* `/stats/attributions` is using a FunctionalEntityFilter now which only allows `INDIVIDUAL` and `PROJECT`
* [EI-810] Added `GET/PATCH /account/user_settings{/keys}` endpoints
* [EI-1482] Added new optional request parameter `conceptSource` to `/search/typeahead` to retrieve specific concepts
* [EI-1193] Added new call to remove Expertise, DELETE `/expertises/{expertiseId}`
* [EI-1535] added `includeSuggestions` parameter to `/stats/projects/**` and `/stats/individuals/**`

### Fixed
* [EI-1527] Fixed `/stats/attribution` pagination. New fields were added: `pageNumber`, `pages`, `size`, `total`
* [EI-1521] `/stats/attributions` will throw 400 when an unsupported entityType filter was provided; added 400 response to this endpoint
* [EI-1531] Corrected the description and summary of API
* [EI-1534] Added missing response body for `/expertises/related`

### Changed
* [EI-1499] Modified specs for `/account/set_password`, `/account/password_reset` and `/account/signup`: the content-type changed from `application/x-www-form-urlencoded` to `application/json;charset=UTF-8`
* [EI-1526] Removed `filter` from specs for `/attributions/suggestions/search/individuals` and `/attributions/suggestions/search/projects`
* [EI-1560] POST `/expertises` creation of Expertise modified to include `isAcronym` related changes
* [EI-1620] Client-Credential authorized consumers will receive a 403 when calling `/account/info`
* [EI-1624] Changing Account-Roles will now revoke all active access-tokens of the respective account. The now enabled refresh-token can be used to fetch a new access-token with the changed roles.
* [EI-1625] Removed `title` and `isTitleAcronym` property and made `titlesByLanguage` required for the request when creating the Expertise POST `/expertises`
* [EI-1688] Textual representation of account-roles is now lowercase and without ROLE_ prefix everywhere
* Output specification for `/expertises/related` changed from single object to an array.
* `/account/info` and `/admin/accounts/{username}/info` had a field `login` which was renamed to `username`


## [v3.1.0-65] - 2018-10-22
### Added
* API endpoints similar to v2.6 `/stats/entities` for `individuals` and `projects`:
    * `POST /stats/individuals` to return counts on attributed expertise and project memberships of Individuals
    * `POST /stats/individuals/{individualId}` to return counts on attributed expertise and project memberships of a specific Individual
    * `POST /stats/projects` to return counts on attributed expertise and members of Projects
    * `POST /stats/projects/{projectId}` to return counts on attributed expertise and members of a specific project
* API endpoint similar to `/stats/entities` for `attributions`:
    * `POST /stats/attributions` to return counts over entities attributed to expertises
* Manual ordering of Project and Individual attributions by `rank` property. It is now possible to change the order of attributions manually by providing an absolute position in the `rank` property when creating or modifying expertise attributions to a Project or an Individual. The order will also influence the relevance of the attribution. The higher the rank the less relevant the attribution.
    * PUT `/attributions/individuals/{individualId}` to replace all existing attributions for an individual
    * PUT `/attributions/projects/{projectId}` to rpelace all existing attributions for a project
    * PATCH `/attributions/individuals/{individualId}/expertises/{expertiseId}` to modify a single expertise attribution of an individual
    * PATCH `/attributions/projects/{projectId}/expertises/{expertiseId}` to modify a single expertise attribution of a project
* Optional storing of skill proficiency level in Project and Individual expertise attributions as a numeric value to indicate the quality of an expertise attribution (see data models below for details).
* Filtered listing of Project and Individual attributions
    * POST `/attributions/search/expertises/{expertiseId}/individuals`
    * POST `/attributions/search/expertises/{expertiseId}/projects`
* Data models:
    * `AccountsList` to represent a list of accounts
    * `DisplayGroupFilter` for filtering results by display group and proficiency level
    * the enumeration `Proficiency` to represent a human readable interpretation of the numeric values of the `proficiencyLevel`:
        * `0` - `UNKNOWN`
        * `1` - `FUNDAMENTAL`
        * `2` - `NOVICE`
        * `3` - `INTERMEDIATE`
        * `4` - `ADVANCED`
        * `5` - `EXPERT`
    * `UpdateAttributionDto` to represent all properties for creating or updating an expertise attribution for a Project or Individual

### Changed
* default value for page size globally to 10
* response of `/admin/accounts` to `AccountsList` (see above)
* signup request parameters unified to camel-casing `first_name` to `firstName` and `last_name` to `lastName`
* `/account/activate` changed parameter `id` to `email`
* Data models:
    * `AttributionIndividualMappedResponse` to be `nullable`
    * `AttributionIndividualResponse` to return attribution `proficiencyLevel` (inicating a manually set skill proficiency) and `projectEndorsementsByGroup` (representing the number of projects of the individual that reference this expertise concept)
    * `AttributionProjectMappedResponse` to be `nullable`
    * `AttributionIndividualResponse` to return attribution `proficiencyLevel` (inicating a manually set skill proficiency)
    * added property `proficiencyLevel` to `IndividualExpertiseAttributionInput` and `ProjectExpertiseAttributionInput` to let a user provide the skill proficiency of an expertise attribution as a numeric value. A value of 0 indicates an unknown proficiency, 1 the lowest proficiency and 5 the highest proficiency.
    * added property `rank` to `IndividualExpertiseAttributionInput` and `ProjectExpertiseAttributionInput` to let a user change the absolut position among all attributions. Providing a `rank` of 1 means the expertise should be listed at the top of the list of attributions, while not providing any rank will append the attribution to the end.
    * the top-level property in `AttributionStatsResponse` from `data` to `groupedStats` for better semantics
    * renamed nested property `totalCount` to `totalStatsCount` in `AttributionStatsResponse` for better semantics
    * casing of the values in property `entityType` of `EntityFilter`, `EntityType`, `FunctionalFilter` and `FunctionalFilterRequiredEntityType`
    * renamed property `totalCount`to `totalGroupCount` in `GroupedTypeStats`


## [v3.0.0-63] - 2018-10-08
### Added
* `/stats/attributions` to obtain count-based stats over expertise concepts attributed to entities
* `/stats/entities` to obtain count-based stats over all entities
* `/stats/individuals` to obtain count-based stats over expertise concepts attributed to individuals
* `/stats/individuals/{individualId}` to obtain count-based stats over expertise concepts attributed to a specific individual
* `/stats/projects` to obtain count-based stats over expertise concepts attributed to projects
* `/stats/projects/{projectId}` to obtain count-based stats over expertise concepts attributed to a specific project
* display groups filter to `ExpertiseIdsFilter`
* `GroupedTypeStats` to reflect attribution counts by entity display group
* `IndividualSearchFilter` and `ProjectSearchFilter` to filter entity results by entity id and entity display groups

### Changed
* request object in `/attributions/suggestions/search/individuals` from `IndividualIdsFilter` to `IndividualSearchFilter`
* request object in `/attributions/suggestions/search/projects` from `ProjectIdsFilter` to `ProjectSearchFilter`
* request object in `/memberships/search/individuals` from `IndividualIdsFilter` to `IndividualSearchFilter`
* request object in `/memberships/search/projects` from `ProjectIdsFilter` to `ProjectSearchFilter`
* replaced field `TypeStat` in `AttributionStatsResponse` by `GroupedTypeStats`
* `StatsEntitiesResponse` defined directly instead of the embedded `TypeStat` object
* renamed field `displayGroups` to `displayGroup` and field `totalCount` to `count` in `TypeStat`


## [v3.0.0-SNAPSHOT-56] - 2018-10-02
### Changed
* The Specification file was converted to **OpenAPI 3.0** schema
* Splited API `/expertises/{expertiseId}/connections` for `individuals` and `projects` and http operation from GET to POST:
    * `/individuals/search/expertises/{expertiseId}`
    * `/projects/search/expertises/{expertiseId}`
* Below API endpoints where renamed:
    * `/batch/individuals/{individualId}/attributeBulkExpertise` => `/attributions/batch/individuals/{individualId}/attributeBulkExpertise`
    * `/index/recommendations/entities` => `/recommendations/entities`
    * `/index/recommendations/expertises` => `/recommendations/expertises`
    * `/projects/{projectId}/memberships` => `/memberships/search/projects/{projectId}`
    * `/projects/{projectId}/memberships/{individualId}` => `/memberships/projects/{projectId}/{individualId}`
    * `/individuals/{individualId}/expertises` => `/attributions/individuals/{individualId}`
    * `/individuals/{individualId}/expertises/{expertiseId}` => `/attributions/{expertiseId}/individuals/{individualId}`
    * `/individuals/{individualId}/projects` => `/memberships/search/individuals/{individualId}`
    * `/individuals/{individualId}/recommendations` => `/recommendations/individuals/{individualId}`
    * `/projects/{projectId}/expertises` => `/attributions/projects/{projectId}`
    * `/projects/{projectId}/expertises/{expertiseId}` => `/attributions/{expertiseId}/projects/{projectId}`
    * `/projects/{projectId}/recommendations` => `/recommendations/projects/{projectId}`
    * `/stats/expertises` => `/stats/attributions`
    * `/individuals/refresh` => `/attributions/individuals/refresh`
    * `/projects/refresh` => `/attributions/projects/refresh`
* Changed listing endpoints to searching endpoints and moved complex filters to request body
    * `GET /expertises` => `POST /expertises/search`
    * `GET /individuals` => `POST /individuals/search`
    * `GET /projects` => `POST /projects/search`
    * `GET /recommendations/expertises` => `POST /recommendations/expertises`
    * `GET /recommendations/individuals` => `POST /recommendations/individuals`
    * `GET /recommendations/projects` => `POST /recommendations/projects`
    * `GET /attributions/expertises` => `POST /attributions/expertises`
    * `GET /attributions/individuals` => `POST /attributions/individuals`
    * `GET /attributions/projects` => `POST /attributions/projects`
    * `Suggestions end points`

* Most PUT calls were changed to PATCH calls updating only provided key-value pairs
    
* `atributedThrough` properties were renamed to `attributionType` and simplified to three possible values:
    * `SUGGESTED` for Attributions that were added as a suggestion by the system to the expertise profile of a Project or Individual based on existing data (text analysis, project memberships)
    * `ACCEPTED` for Attributions that were accepted by a human user to be part of the profile of an Individual or Project
    * `REJECTED` for Attributions that were rejected by a human user to be excluded from the profile of an Individual or Project; future automatic suggestions will be ignored in that case

* **all** `/account/*` calls will now have the consumption mime-type `application/json;charset=UTF-8` instead of `application/x-www-form-urlencoded`
 
### New
* Creational Bulk calls will reply with HTTP status code `207 Multi Status` stating the success or failure status of sub-requests.
* 

### Added
* Added `/attributions/search/individuals`, a search operation for expertises using ids of individuals
* Added `/attributions/search/projects`, a search operation for expertises using ids of projects
* Added `/attributions/search/expertises/individuals` to search for attribution-individuals based on given metaFilter and expertiseIds
* Added `/attributions/search/expertises/projects` to search for attribution-projects based on given metaFilter and expertiseIds

### Removed
* Removed attribute `affiliation` from `Project` and `Individual` request and response models
* Removed detailed information from nested expertises
* Removed `relatedExpertises`, `expertises`, `memberships` and `stats` from nested objects in responses
* Removed detailed info like `description`, `geolocation`, `contactData` and `dataSources` from `Individual` and `Project` response objects, except API calls for getting the detailed info.
    * `/individuals/{individualId}`
    * `/projects/{projectId}`
* Removed individual visibility for the time being
* Temporarily removed all `stats` endpoints

## [v2.6.0] - 2018-08-10
### Fixed
* Added missing consumes attribute for `/individuals` and `/projects` paths

### Changed
* Attribution Reason enum values and acceptable list of values in both requests and responses, and attributedThrough attribute in responses. Affected Urls are:
	* `/projects/{projectId}/expertises/{expertiseId}`
	* `/projects/{projectId}/expertises`
	* `/projects/{projectId}`
	* `/individuals/{individualId}/expertises/{expertiseId}`
	* `/individuals/{individualId}/expertises`
	* `/individuals/{individualId}`
* Changed specs for below endpoints omitting the request body
    * `PATCH /individuals/{individualId}/expertises/{expertiseId}`
    * `PATCH /projects/{projectId}/expertises/{expertiseId}`
    * Added `extKnowledgeBaseId` to `ExpertiseDto` and  `CreateExpertiseDto` definitions
* Response object from Expertise Concept Detection call now contains a `mentions` field that refers to the entered CSV keywords from the input

### Added
* added `/expertises/related?expertiseIds=...` call to fetch related Expertises
* Added more example values in the entire specification
* Added `PUT/PATCH /individuals/{individualId}/expertises/{expertiseId}` to modify attributions
* Added `PUT/PATCH /projects/{projectId}/expertises/{expertiseId}` to modify attributions
* `GET/DELETE /individuals/{individualId}/meta?keys=...` parameter was introduced to filter meta entries by keys
* `GET/DELETE /projects/{projectId}/meta?keys=...` parameter was introduced to filter meta entries by keys

### Removed
* Expertise Concepts have no `relatedExpertises` field anymore. Please use the new `/expertises/related` call to fetch them separately if needed.


## [v2.5.1] - 2018-07-31

### Changed
* Oauth token revocation is now at `DELETE /tokens`

## [v2.5.0] - 2018-07-31
### Added
* Create, read and modify Project and Individual Meta Data
* Added `?metaFilter` for multiple `GET` calls; It accepts a JSON object containing Key-Value pairs by which a filtering on Meta Data will be performed:
	* `GET /expertises/{expertiseId}/connections`
	* `GET /individuals`
	* `GET /individuals/{individualId}/recommendations`
	* `GET /projects`
	* `GET /projects/{projectId}/recommendations`
	* `GET /index/recommendations/entities`
* *Expertimental:* Endpoints to refresh Individual and Project Expertise profiles `/individuals/refresh` and `/projects/refresh`
* *Expertimental:* Added query parameter to enable/disable inheritance for deattribution of Projects

### Fixed
* Deleting an Individual will also delete the Account if there is one; The API will refuse to delete the profile of the currently logged in user, though.
* Duplicate Memberships in `POST /projects/{projectId}/memberships` call will produce proper validation error.
* List Expertises from Projects or Individuals with non-existing entity id will respond with `[404 Not Found]` instead of returning empty Expertises list

### Changed
* Project-Attribution will return Status `[201 Created]` instead of `[204 No Content]`
* Individual-Attribution will return Status `[201 Created]` instead of `[204 No Content]`


## [v2.4.1] - 2018-07-17
### Changed
* For typeahead response created a new definition for typeahead expertise dto

## [v2.4.0] - 2018-06-29
### Fixed
* Newly created Individuals and Project will always be listed first

### Added
* New endpoint `POST /expertises` added to create new Expertise Concept.
* `/individuals/{id}/expertises` got a new property: `groupedEndorsements` that lists endorsements grouped by the Display Groups of the Projects they are endorsing.

### Changed
* OAuth token URL is now at `/api/oauth/token`
