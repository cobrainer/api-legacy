from collections import OrderedDict
import json
import io


def sort_obj(obj, stack, sort_order=None, post_process_val=(lambda x, s: sort_obj(x, s))):
    try:
        if obj is None or type(obj) in (int, str, bool, float, unicode, list):
            return obj

        if type(obj) is list:
            return sort_array(obj, stack)

        ending_keys = []

        if sort_order is None or sort_order.keys() is None:
            sort_order = general_order

        ret = OrderedDict([])

        for key in [k for k in sort_order.keys() if k in obj and k not in ending_keys]:
            ret.update(
                {key: (sort_order[key] if sort_order[key] is not None else post_process_val)(obj[key], stack + [key])})

        for key in sorted([k for k in obj.keys() if k not in sort_order and k not in ending_keys]):
            ret.update({key: post_process_val(obj[key], stack + [key])})

        for key in ending_keys:
            if key in obj:
                ret.update({key: post_process_val(obj[key], stack + [key])})

        return ret
    except Exception, e:
        print "Error: `" + e.message + "`", "@ json-path:", "(`" + "`>`".join(
            stack) + "`)"
        print "input object:", obj
        print "sort_order:", sort_order
        print "post_process_func @line:", post_process_val.func_code.co_firstlineno
        exit(-1)


def sort_array(obj, stack, post_process=(lambda x, s: sort_obj(x, s))):
    for i, row in enumerate(obj):
        post_process(row, stack + [i])

    obj_ = [post_process(row, stack + [i]) for i, row in enumerate(obj)]
    return obj_


contact_order = OrderedDict([
    ("name", None),
    ("email", None),
    ("url", None)
])
parameters_order = OrderedDict([
    ('description', None),
    ('name', None),
    ('in', None),
    ('required', None),
    ('style', None),
    ('explode', None),
    ('schema', lambda x, s: sort_obj(x, s, schema_order)),
    ('type', None)
])

response_order = OrderedDict([
    ('description', None),
    ('content', lambda x, s: sort_obj(x, s, post_process_val=(
        lambda x, s: sort_obj(x, s, post_process_val=lambda x, s: sort_obj(x, s, schema_order)))))
])
schema_order = OrderedDict([
    ('type', None),
    ('default', None),
    ('example', None),
    ('description', None)
])

x_operation_settings_order = OrderedDict([
    ("CollectParameters", None),
    ("AllowDynamicQueryParameters", None),
    ("AllowDynamicFormParameters", None),
    ("IsMultiContentStreaming", None)
])
http_operation_order = OrderedDict([
    ('tags', None),
    ('title', None),
    ('description', None),
    ('summary', None),
    ('consumes', None),
    ('produces', None),
    ('parameters', lambda x, s: sort_array(x, s, lambda x, s: sort_obj(x, s, parameters_order))),
    ('requestBody', lambda x, s: sort_obj(x, s, response_order)),
    ('responses', lambda x, s: sort_obj(x, s, post_process_val=(lambda x, s: sort_obj(x, s, response_order)))),
    ('security', None),
    ('x-operation-settings', lambda x, s: sort_obj(x, s, x_operation_settings_order))
])
info_order = OrderedDict([
    ("title", None),
    ("description", None),
    ("version", None),
    ("contact", lambda x, s: sort_obj(x, s, contact_order))
])

url_def_order = OrderedDict([
    ('parameters', lambda x, s: sort_array(x, s, lambda x, s: sort_obj(x, s, parameters_order))),
    ('get', lambda x, s: sort_obj(x, s, http_operation_order)),
    ('post', lambda x, s: sort_obj(x, s, http_operation_order)),
    ('put', lambda x, s: sort_obj(x, s, http_operation_order)),
    ('patch', lambda x, s: sort_obj(x, s, http_operation_order)),
    ('delete', lambda x, s: sort_obj(x, s, http_operation_order))
])

general_order = OrderedDict([
    ('id', None),
    ('description', None),
    ('title', None),
    ('name', None)
])

properties_def_order = OrderedDict([
    ('type', None),
    ('items', None),
    ('properties', lambda x, s: sort_obj(x, s, general_order, lambda x, s: sort_obj(x, s, properties_def_order))),
    ('example', None)
])

definitions_def_order = OrderedDict([
    ('title', None),
    ('type', None),
    ('description', None),
    ('properties', lambda x, s: sort_obj(x, s, general_order, lambda x, s: sort_obj(x, s, properties_def_order))),
])

components_order = OrderedDict([
    ('schemas', lambda x, s: sort_obj(x, s, post_process_val=(lambda x, s: sort_obj(x, s, definitions_def_order)))),
    ('securitySchemas', None)
])
root_order = OrderedDict([
    ('openapi', None),
    ('info', lambda x, s: sort_obj(x, s, info_order)),
    ('servers', None),
    ('paths', lambda x, s: sort_obj(x, s, post_process_val=(lambda x, s: sort_obj(x, s, url_def_order)))),
    ('components', lambda x, s: sort_obj(x, s, components_order)),
    ('security', None),
    ('securityDefinitions', None)
])

js = json.dumps(json.load(file("../EIAPI.json")), sort_keys=True)
ud = json.loads(js, object_pairs_hook=OrderedDict)

retUd = sort_obj(ud, [], root_order)

with io.open('../EIAPI.json', 'wb') as out:
    out.write(json.dumps(retUd, indent=2).replace(' \n', '\n'))
    out.flush()

print "`EIAPI.json` was sorted successfully!"
