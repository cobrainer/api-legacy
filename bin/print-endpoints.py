import json


def fill_blanks(str, len):
    return str + " " * (len - str.__len__())


with open('../EIAPI.json') as japi:
    spec = json.load(japi)
    paths = spec['paths']

    keys = sorted(list(paths.keys()))

    maxLen = 0
    for path in keys:
        maxLen = max(maxLen, path.__len__())

    stat = 0
    pathArgs = {}
    maxPathArgsLen = 0
    maxSummaryLen = 0
    for path in keys:
        p = paths[path]

        for k in ['get', 'post', 'patch', 'put', 'delete']:
            if k not in paths[path]:
                continue

            pathArg = "[" + ", ".join(
                map(lambda row: row['name'] + '(' + row['in'] + ')' if row['in'] != "body" else '(payLoad)',
                    paths[path][k].get('parameters', []))) + "]"
            pathArgs.update([(k + " " + path, pathArg)])
            maxPathArgsLen = max(maxPathArgsLen, pathArg.__len__())
            maxSummaryLen = max(maxSummaryLen, p[k]['summary'].__len__())

            stat += 1

    print "\nTotal calls: ", stat

    pref = ''

    for path in keys:
        if pref != path[0:path[1:].find('/')]:
            print

        p = paths[path]

        for k in ['get', 'post', 'patch', 'put', 'delete']:
            if k not in paths[path]:
                continue

            print fill_blanks(p[k]['summary'], maxSummaryLen), \
                fill_blanks(k.upper(), 8), \
                fill_blanks(path, maxLen), \
                fill_blanks(pathArgs[k + " " + path], maxPathArgsLen)

        pref = path[0:path[1:].find('/')]
